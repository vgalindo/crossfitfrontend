import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from './service/login.service';
import {NGXLogger} from 'ngx-logger';
// import { Signale } from 'signale';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  /*private options = {
    scope: 'LoginComponent'
  };
  private debug = new Signale(this.options);*/

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private logger: NGXLogger,
  ) { }

  ngOnInit() {
  }

  loginSubmit() {
    const { loginForm } = this;

    this.logger.info(loginForm.getRawValue());

    this.loginService.authenticate(loginForm.getRawValue()).subscribe();
  }

  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }
}
