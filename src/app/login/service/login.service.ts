import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {TokenService} from '../../common/services/token.service';
import {LoginUserGQLService} from '../../graphql/exercise/mutator/login-user-gql.service';
import {Apollo} from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private logger: NGXLogger,
    private tokenService: TokenService,
    private apollo: Apollo,
    private loginUserGQL: LoginUserGQLService,
  ) { }

  authenticate(user) {
    const { apollo, loginUserGQL, logger } = this;
    const { username, password } = user;

    return apollo.mutate({
      mutation: loginUserGQL.document,
      variables: {username, password}
    }).pipe(
      filter((res: {error?}) => !res.error),
      map( (res: { data?: { loginUser?:  { idToken? } } }) => {
        logger.info('GRAPHQL RESPONSE: ', res);

        const { idToken } = res.data.loginUser;

        this.tokenService.setToken(idToken);

        this.router.navigate(['/exercises']);
      }),
    );
  }
}
