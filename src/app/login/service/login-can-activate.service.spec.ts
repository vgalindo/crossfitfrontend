import { TestBed, inject } from '@angular/core/testing';

import { LoginCanActivateService } from './login-can-activate.service';

describe('LoginCanActivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginCanActivateService]
    });
  });

  it('should be created', inject([LoginCanActivateService], (service: LoginCanActivateService) => {
    expect(service).toBeTruthy();
  }));
});
