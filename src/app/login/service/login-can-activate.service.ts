import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {filter, mapTo} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class LoginCanActivateService implements CanActivate {

  constructor(
    private router: Router,
    private logger: NGXLogger,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return of(localStorage.getItem('sessionToken')).pipe(
      filter(token => {
        if (token) {
          this.router.navigate(['exercises']);
          return false;
        }
        return true;
      }),
      mapTo(true),
    );
  }
}
