import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ExercisesService} from '../exercises/services/exercises.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {NGXLogger} from 'ngx-logger';
import { faSave } from '@fortawesome/free-regular-svg-icons';
import {catchError, map} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {FileUploadGqlService} from '../../graphql/upload/mutator/file-upload-gql.service';
import {Apollo} from 'apollo-angular';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {bind} from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-form-exercises',
  templateUrl: './form-exercises.component.html',
  styleUrls: ['./form-exercises.component.scss']
})
export class FormExercisesComponent implements OnInit {

  faSave = faSave;
  update: boolean = false;
  imgPreview: string;

  exerciseForm = this.formBuilder.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    pictureName: [null],
    pictureData: [null],
    video: ['', Validators.required]
  });
  constructor(
    private formBuilder: FormBuilder,
    private exercisesService: ExercisesService,
    private logger: NGXLogger,
    private route: ActivatedRoute,
    private fileUploadGql: FileUploadGqlService,
    private apollo: Apollo,
    private http: HttpClient,
    private changeDetection: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.route.data.pipe(
      map( data => {
        this.logger.info('FormExercises', data);

        if (data[0]) {
          this.update = true;
          this.exerciseForm.addControl('id', new FormControl(data[0].id, [Validators.required]));
          this.name.setValue(data[0].name);
          this.description.setValue(data[0].description);
          this.imgPreview = data[0].picture;
          this.video.setValue(data[0].video);
        }
      })
    ).subscribe();
  }

  exerciseSubmit() {
    const { exerciseForm, update, logger } = this;

    /*logger.info(exerciseForm.get('picture').value);*/
    logger.info(exerciseForm.getRawValue());

    if (update) {
      this.exercisesService.updateExercise(exerciseForm.getRawValue()).subscribe();
    } else {
      this.exercisesService.addExercise(exerciseForm.getRawValue()).subscribe();
    }
  }

  get name() { return this.exerciseForm.get('name'); }
  get description() { return this.exerciseForm.get('description'); }
  get picture() { return this.exerciseForm.get('picture'); }
  get video() { return this.exerciseForm.get('video'); }

  uploading(file) {
    const {fileUploadGql, apollo, logger} = this;
    const { files } = file;

    logger.info('file', files[0]);
    const fileToUpload: File = files[0];
    const formData = new FormData();
    formData.append('files', fileToUpload, fileToUpload.name);
    this.http.post('http://localhost:4000/api/v1/upload', formData).subscribe(
      () => {
        const exercise = {id: this.exerciseForm.get('id').value, picture: fileToUpload.name};
        this.exercisesService.updateExercise(exercise).subscribe();
      }
    );

  }

  onPictureChnge(event) {
    const reader = new FileReader();

    if ( event.target.files && event.target.files.length ) {
      const [file]  = event.target.files;
      this.logger.info('event.target.files - ', event.target.files);
      reader.readAsBinaryString(file);
      this.exerciseForm.patchValue({pictureName: file.name});
    }
    reader.onload = () => {
      this.exerciseForm.patchValue({pictureData: reader.result });
    };
    this.changeDetection.markForCheck();
  }
}
