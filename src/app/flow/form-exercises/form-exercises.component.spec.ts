import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormExercisesComponent } from './form-exercises.component';

describe('FormExercisesComponent', () => {
  let component: FormExercisesComponent;
  let fixture: ComponentFixture<FormExercisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormExercisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormExercisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
