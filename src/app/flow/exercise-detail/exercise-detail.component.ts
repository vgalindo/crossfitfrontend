import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';

@Component({
  selector: 'app-exercise-detail',
  templateUrl: './exercise-detail.component.html',
  styleUrls: ['./exercise-detail.component.scss']
})
export class ExerciseDetailComponent implements OnInit {

  exerciseDetail$;

  constructor(
    private route: ActivatedRoute,
    private logger: NGXLogger,
  ) { }

  ngOnInit() {
    const { route, logger } = this;
    this.exerciseDetail$ = route.data.pipe(
      map( data => {
        return data[0];
      })
    );
  }

}
