import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {FindExerciseGqlService} from '../../../graphql/exercise/queries/find-exercise-gql.service';
import {Apollo, } from 'apollo-angular';
import {take, map, catchError} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';
import {isNullOrUndefined} from 'util';
import {ExercisesService} from '../../exercises/services/exercises.service';

@Injectable({
  providedIn: 'root'
})
export class ExerciseDetailsResolverService implements Resolve<any>{

  constructor(
    private findExerciseGql: FindExerciseGqlService,
    private exercisesService: ExercisesService,
    private router: Router,
    private apollo: Apollo,
    private logger: NGXLogger,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    const { router, apollo, findExerciseGql, exercisesService, logger } = this;
    logger.log(`route.paramMap.get('id') - ${JSON.stringify(route.paramMap)}`);
    const id = route.paramMap.get('id');
    logger.info('Resolver-', id);

    if (!id) {
      logger.log(`Id is ${id}, i need to return a new registry`);
      return exercisesService.addExercise({});
    }

    return apollo.query<any>({
      query: findExerciseGql.document,
      variables: { id }
    }).pipe(
      take(1),
      map(response => {
        const { data } = response;

        if (!isNullOrUndefined(data.exercises)) {
          return data.exercises;
        } else {
          router.navigate(['exercises']);
          return null;
        }

      }),
      catchError(error => {
        // this.logger.error('Resolver-', error );
        router.navigate(['exercises']);
        return of(error);
      })
    );
  }
}
