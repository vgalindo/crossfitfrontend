import { TestBed, inject } from '@angular/core/testing';

import { ExerciseDetailsResolverService } from './exercise-details-resolver.service';

describe('ExerciseDetailsResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExerciseDetailsResolverService]
    });
  });

  it('should be created', inject([ExerciseDetailsResolverService], (service: ExerciseDetailsResolverService) => {
    expect(service).toBeTruthy();
  }));
});
