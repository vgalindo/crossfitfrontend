import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {filter, mapTo} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ExercisesCanActivateService implements CanActivate {

  constructor(
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return of(localStorage.getItem('sessionToken'))
      .pipe(
        filter(token => {
          if (!token) {
            this.router.navigate(['/']);
            return false;
          }
          return true;
        }),
        mapTo(true),
      );
  }

}
