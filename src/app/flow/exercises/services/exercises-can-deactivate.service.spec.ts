import { TestBed, inject } from '@angular/core/testing';

import { ExercisesCanDeactivateService } from './exercises-can-deactivate.service';

describe('ExercisesCanDeactivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExercisesCanDeactivateService]
    });
  });

  it('should be created', inject([ExercisesCanDeactivateService], (service: ExercisesCanDeactivateService) => {
    expect(service).toBeTruthy();
  }));
});
