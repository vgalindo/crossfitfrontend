import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot} from '@angular/router';
import {T} from '@angular/core/src/render3';
import {Observable} from 'rxjs';
import {FormExercisesComponent} from '../../form-exercises/form-exercises.component';
import {NGXLogger} from 'ngx-logger';
import {ExercisesService} from './exercises.service';
import {map, mapTo, take} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {nextTick} from 'async';

@Injectable({
  providedIn: 'root'
})
export class ExercisesCanDeactivateService implements CanDeactivate<FormExercisesComponent> {

  constructor(
    private logger: NGXLogger,
    private exercisesService: ExercisesService,
  ) { }

  canDeactivate(component: FormExercisesComponent,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const { logger, exercisesService } = this;
    const id = component.exerciseForm.get('id').value;
    return exercisesService.findExercise(id).pipe(
      take(1),
      map(async (data: any) => {
        logger.info(`finding if name false, delete it ${JSON.stringify(data)}`);
        if (isNullOrUndefined(data.name)) {
          await exercisesService.deleteExercise(data.id).pipe(
            take(1),
          ).subscribe();
        }
      }),
      mapTo(true),
    );

     /*nextTick(() => true);
    return true;*/
  }
}
