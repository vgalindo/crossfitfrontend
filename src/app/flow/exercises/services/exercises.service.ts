import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {catchError, map, take, tap} from 'rxjs/operators';
import {TokenService} from '../../../common/services/token.service';
import {Apollo} from 'apollo-angular';
import {ExercisesGQLService} from '../../../graphql/exercise/queries/exercises-gql.service';
import {FindExerciseGqlService} from '../../../graphql/exercise/queries/find-exercise-gql.service';
import {AddExerciseGqlService} from '../../../graphql/exercise/queries/add-exercise-gql.service';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {UpdateExercisesGqlService} from '../../../graphql/exercise/queries/update-exercises-gql.service';
import {DeleteExercisesGqlService} from '../../../graphql/exercise/mutator/delete-exercises-gql.service';

@Injectable({
  providedIn: 'root'
})
export class ExercisesService {

  constructor(
    private http: HttpClient,
    private logger: NGXLogger,
    private tokenService: TokenService,
    private apollo: Apollo,
    private exercisesGQL: ExercisesGQLService,
    private findExerciseGql: FindExerciseGqlService,
    private addExerciseGql: AddExerciseGqlService,
    private updateExercisesGql: UpdateExercisesGqlService,
    private deleteExercisesGql: DeleteExercisesGqlService,
    private router: Router,
  ) {
  }

  getExercises() {
    // this.http.get('http://localhost:4000/api/v1/exercises').subscribe();
    const {exercisesGQL, apollo, logger} = this;

    return apollo.watchQuery({
      query: exercisesGQL.document,
      fetchPolicy: 'network-only',
    }).valueChanges.pipe(
      tap(() => {}),
      map((res: { data: { allExercises } }) => {
        logger.info('getting Exercises', res.data.allExercises);
        return res.data.allExercises;
      } )
    );
  }

  findExercise(id) {
    const {findExerciseGql, apollo, logger} = this;

    return apollo.watchQuery({
      query: findExerciseGql.document,
      variables: {id}
    }).valueChanges.pipe(
      map((res: { data: { exercises } }) => res.data.exercises)
    );
  }

  addExercise(exercise) {
    const {addExerciseGql, apollo, logger, router} = this;
    const {name, description, picture, video} = exercise;

    return apollo.mutate<any>({
      mutation: addExerciseGql.document,
      variables: {name, description, picture, video}
    }).pipe(
      catchError(error => {
        logger.error('addExercise-', error);
        return of(error);
      }),
      map(data => {
        const { addExercise } = data.data;
        logger.info(data);
        return router.navigate([`upsert/exercises/${addExercise.id}`]);

      })
    );
  }

  updateExercise(exercise: any) {
    const {updateExercisesGql, apollo, logger, router} = this;
    const {id, name, description, pictureName, pictureData, video} = exercise;

    if (pictureName && pictureData) {
      logger.info(id, pictureName, pictureData);
      this.uploading(id, pictureName, pictureData);
    }
    logger.info('BEFORE MUTAING');
    return apollo.mutate({
      mutation: updateExercisesGql.document,
      variables: {id, name, description, video}
    }).pipe(
      catchError(error => {
        logger.error('updateExercise-', error);
        return of(error);
      }),
      map(data => {
        const { updateExercise } = data.data;
        logger.info(data);
        router.navigate([`exercises/${updateExercise.id}`]);

      })
    );
  }

  deleteExercise(id: any) {
    const {deleteExercisesGql, apollo, logger, router} = this;

    return apollo.mutate({
      mutation: deleteExercisesGql.document,
      variables: {id}
    }).pipe(
      catchError(error => {
        logger.error('deleteExercise-', error);
        return of(error);
      }),
      map(data => {
        const { deleteExercise } = data.data;
        logger.info(data);
        // router.navigate([`exercises/${deleteExercise.id}`]);

      })
    );
  }

  uploading(id, fileName, fileData) {
    // const { logger, updateExercise} = this;
    this.logger.info('uploading...');
    this.logger.info('file', typeof fileData);
    /*const { files } = file;

    logger.info('file', files[0]);
    const fileToUpload: File = files[0];*/
    const formData = {fileData, fileName};
    // formData.append('files', fileData, fileName);

    this.http.post('http://localhost:4000/api/v1/upload', formData).pipe(take(1)).subscribe(
      () => {
        const exercise = {id, picture: fileName};
        this.updateExercise(exercise).pipe(take(1)).subscribe();
      }
    );
  }

}
