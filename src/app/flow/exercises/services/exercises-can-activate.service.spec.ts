import { TestBed, inject } from '@angular/core/testing';

import { ExercisesCanActivateService } from './exercises-can-activate.service';

describe('ExercisesCanActivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExercisesCanActivateService]
    });
  });

  it('should be created', inject([ExercisesCanActivateService], (service: ExercisesCanActivateService) => {
    expect(service).toBeTruthy();
  }));
});
