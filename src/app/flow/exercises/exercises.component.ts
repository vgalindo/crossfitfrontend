import { Component, OnInit } from '@angular/core';
import {ExercisesService} from './services/exercises.service';
import {Observable} from 'rxjs';
import {ExercisesGQLService} from '../../graphql/exercise/queries/exercises-gql.service';
import {Apollo} from 'apollo-angular';
import {catchError, delay, map, take} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';
import {Router} from '@angular/router';
import { faPlusSquare, faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss']
})
export class ExercisesComponent implements OnInit {

  faPlusSquare = faPlusSquare;
  faPencilAlt = faPencilAlt;
  faTrashAlt = faTrashAlt;
  exercises$: Observable<any>;
  exercisesGQL$: Observable<any>;

  constructor(
    private logger: NGXLogger,
    private router: Router,
    private exercisesService: ExercisesService,

  ) { }

  ngOnInit() {
    this.getAllExercices();
  }
  getAllExercices() {
    this.exercisesGQL$ = this.exercisesService.getExercises().pipe(
      // map(data => this.logger.info('ExercisesComponent - ngOnInit', data))
    );
  }

  getExercise(id) {
    this.router.navigate([`exercises/${id}`]);
    // this.exercisesService.findExercise(id).subscribe();
  }

  newExerciseFrom() {
    this.router.navigate(['upsert/exercises']);
  }

  updateExerciseFrom(id) {
    this.logger.info(`upsert/exercises/${id}`);
    this.router.navigate([`upsert/exercises/${id}`]);
  }

  deleteExerciseFrom(id: any) {
    this.exercisesService.deleteExercise(id).pipe(take(1)).subscribe();
    this.getAllExercices();
  }
}
