import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {ExercisesComponent} from './flow/exercises/exercises.component';
import {ExercisesCanActivateService} from './flow/exercises/services/exercises-can-activate.service';
import {LoginCanActivateService} from './login/service/login-can-activate.service';
import {ExerciseDetailComponent} from './flow/exercise-detail/exercise-detail.component';
import {ExerciseDetailsResolverService} from './flow/exercise-detail/services/exercise-details-resolver.service';
import {FormExercisesComponent} from './flow/form-exercises/form-exercises.component';
import {ExercisesCanDeactivateService} from './flow/exercises/services/exercises-can-deactivate.service';

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [LoginCanActivateService]},
  {path: 'signup', component: SignupComponent, canActivate: [LoginCanActivateService]},
  {path: 'exercises', component: ExercisesComponent, canActivate: [ExercisesCanActivateService]},
  {path: 'add/exercises', component: FormExercisesComponent, canActivate: [ExercisesCanActivateService]},
  {path: 'upsert/exercises', component: FormExercisesComponent, canActivate: [ExercisesCanActivateService], canDeactivate: [ExercisesCanDeactivateService], resolve: [ExerciseDetailsResolverService]},
  {path: 'upsert/exercises/:id', component: FormExercisesComponent, canActivate: [ExercisesCanActivateService], canDeactivate: [ExercisesCanDeactivateService], resolve: [ExerciseDetailsResolverService]},
  {path: 'exercises/:id', component: ExerciseDetailComponent, canActivate: [ExercisesCanActivateService], resolve: [ExerciseDetailsResolverService]},

  {path: '', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
