import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {FooterComponent} from './common/footer/footer.component';
import {NavbarComponent} from './common/navbar/navbar.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ExercisesComponent} from './flow/exercises/exercises.component';
import {ExerciseDetailComponent} from './flow/exercise-detail/exercise-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpHeaders} from '@angular/common/http';
import {LoggerModule, NGXLogger, NgxLoggerLevel} from 'ngx-logger';
import {Apollo, ApolloBoost, ApolloBoostModule, Operation, InMemoryCache, ApolloLink} from 'apollo-angular-boost';
import { setContext } from 'apollo-link-context';
import {TokenService} from './common/services/token.service';
import {Router} from '@angular/router';
import {ErrorResponse, onError} from 'apollo-link-error';
import {DefinitionNode} from 'graphql';
import { FormExercisesComponent } from './flow/form-exercises/form-exercises.component';
import {HttpLink} from 'apollo-angular-link-http';
import { createUploadLink } from 'apollo-upload-client';
import {AppHttpInterceptorService} from './core/services/app-http-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    FooterComponent,
    NavbarComponent,
    ExercisesComponent,
    ExerciseDetailComponent,
    FormExercisesComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ApolloBoostModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.ERROR
    }),
    AppRoutingModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    boost: ApolloBoost,
    tokenService: TokenService,
    private logger: NGXLogger,
    private router: Router,
    apollo: Apollo,
    httpLink: HttpLink,
  ) {
    boost.create({
      uri: `http://localhost:4000/graphql`,
      request: async (operation: Operation) => {
        const { name } = operation.query.definitions['0'];
        logger.info(name);

        if (name.value !== 'loginUser' && name.value !== 'signupUser') {
          const token = localStorage.getItem('sessionToken');
          operation.setContext({
            headers: {
              authorization: token
            }
          });
        }
      },
      onError: (errorObj: ErrorResponse) => {
        // logger.error( errorObj.response );
        let errorRes;
        if (errorObj.networkError) {
          // @ts-ignore
          const { error } = errorObj.networkError;
          errorRes = error;
        } else {
          // @ts-ignore
          const { errors } = errorObj.response;
          errorRes = errors[0];
        }
        // logger.error('errorRes-',errorRes);
        switch (errorRes.message) {
          case 'Wrong Credentials!! please check your data':
            localStorage.clear();
            // router.navigate(['/']);
            break;
          case 'Expired token':
            localStorage.clear();
            router.navigate(['/']);
            break;
          case 'Invalid token':
            localStorage.clear();
            router.navigate(['/']);
            break;
          default:
            break;
        }
      }
    });

    /*const http = createUploadLink({
      uri: `http://localhost:4000/graphql`,
    });

    const middleware = setContext(async (context) => {
        const { operationName } = context;
        logger.info(operationName);

        if (operationName !== 'loginUser' && operationName !== 'signupUser') {
          const token = localStorage.getItem('sessionToken');
          logger.info(token);
          /!* EXTREMELY IMPORTANT TO SEND A NORMAL OBJECT WITH HEADERS AND NOT A HTTPHEADERS INSTANCE!!! *!/
          return { headers: {'Authorization': token} };
        }
        return { headers: null };
    });

    const error = onError((networkError) => {
      // logger.error( networkError );
      let errorRes;
      if (networkError.graphQLErrors) {
        // @ts-ignore
        const { graphQLErrors } = networkError;
        errorRes = graphQLErrors[0].message;
      } else if (networkError.response) {

        // @ts-ignore
        const { errors } = networkError.response;
        errorRes = errors[0];
      } else {
        return;
      }
      // logger.error('errorRes-',errorRes);
      switch (errorRes.message) {
        case 'Invalid token':
        case 'Expired token':
        case '`auth` property not found on context!':
          localStorage.clear();
          router.navigate(['/']);
          break;
        case 'Wrong Credentials!! please check your data':
          localStorage.clear();
          // router.navigate(['/']);
          break;
        default:
          break;
      }
    });
    const link = ApolloLink.from([error, middleware, http]);
    apollo.create({
      link,
      cache: new InMemoryCache()
    });*/
  }
}
