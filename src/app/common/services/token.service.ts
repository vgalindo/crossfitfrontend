import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(
    private logger: NGXLogger,
  ) { }

  retrieveToken() {
    this.logger.info('sesionToken', localStorage.getItem('sessionToken'));
    return new HttpHeaders({
      'Authorization': localStorage.getItem('sessionToken'),
    });
  }

  setToken(token) {
    localStorage.setItem('sessionToken', `${token}`);
  }
}
