import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NGXLogger} from 'ngx-logger';
import {tap} from 'rxjs/operators';

@Injectable()
export class AppHttpInterceptorService implements HttpInterceptor {

  constructor(
    private logger: NGXLogger,
  ) {
    console.log('AppHttpInterceptorService - hi im here ');
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { logger } = this;
    logger.info('http-intercepted - ', req);

    return next.handle(req).pipe(
      tap( event => {
        if (event instanceof HttpResponse) {
          logger.info('response-intercepted - ', event);
          console.log('response-intercepted - ', event);
        }
      })
    );
  }
}
