import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {filter, map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(
    private http: HttpClient,
    private logger: NGXLogger,
    private router: Router,
  ) { }

  newUser(user) {
    this.http.post<{error?, user?}>('api/v1/users', user).pipe(
      filter(res => !res.error),
      map(res => this.router.navigate(['login']))
    ).subscribe();
  }
}
