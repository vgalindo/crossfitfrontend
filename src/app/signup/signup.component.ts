import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormBuilder, Validators} from '@angular/forms';
import {NGXLogger} from 'ngx-logger';
import {SignupService} from './services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm = this.formBuilder.group({
    names: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    user: ['', Validators.required],
    pass: ['', Validators.required],
  });

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private logger: NGXLogger,
    private signupService: SignupService,
  ) { }

  ngOnInit() {
  }

  signupSubmit() {
    const { signupForm } = this;

    this.signupService.newUser(signupForm.getRawValue());
  }

  get names() { return this.signupForm.get('names'); }
  get email() { return this.signupForm.get('email'); }
  get user() { return this.signupForm.get('user'); }
  get pass() { return this.signupForm.get('pass'); }
}
