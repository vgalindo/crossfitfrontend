import { Injectable } from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class FileUploadGqlService extends Mutation {
  document = gql`
    mutation singleUpload($file: Upload!){
      singleUpload(file: $file){
        filename
      }
    }
  `;
}
