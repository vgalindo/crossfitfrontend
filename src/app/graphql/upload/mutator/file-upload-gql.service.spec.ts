import { TestBed, inject } from '@angular/core/testing';

import { FileUploadGqlService } from './file-upload-gql.service';

describe('FileUploadGqlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileUploadGqlService]
    });
  });

  it('should be created', inject([FileUploadGqlService], (service: FileUploadGqlService) => {
    expect(service).toBeTruthy();
  }));
});
