import { TestBed, inject } from '@angular/core/testing';

import { LoginUserGQLService } from './login-user-gql.service';

describe('LoginUserGQLService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginUserGQLService]
    });
  });

  it('should be created', inject([LoginUserGQLService], (service: LoginUserGQLService) => {
    expect(service).toBeTruthy();
  }));
});
