import { Injectable } from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class LoginUserGQLService extends Mutation {
  document = gql`
    mutation loginUser($username: String, $password: String){
      loginUser(username: $username, password: $password){
        idToken
      }
    }
  `;
}
