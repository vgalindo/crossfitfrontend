import { Injectable } from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class DeleteExercisesGqlService extends Mutation {
  document = gql`
    mutation deleteExercises($id: String) {
      deleteExercise(id: $id) {
        id
        name
      }
    }
`;
}
