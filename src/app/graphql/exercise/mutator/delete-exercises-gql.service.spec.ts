import { TestBed, inject } from '@angular/core/testing';

import { DeleteExercisesGqlService } from './delete-exercises-gql.service';

describe('DeleteExercisesGqlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteExercisesGqlService]
    });
  });

  it('should be created', inject([DeleteExercisesGqlService], (service: DeleteExercisesGqlService) => {
    expect(service).toBeTruthy();
  }));
});
