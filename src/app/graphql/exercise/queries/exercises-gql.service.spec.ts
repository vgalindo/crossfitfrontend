import { TestBed, inject } from '@angular/core/testing';

import { ExercisesGQLService } from './exercises-gql.service';

describe('ExercisesGQLService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExercisesGQLService]
    });
  });

  it('should be created', inject([ExercisesGQLService], (service: ExercisesGQLService) => {
    expect(service).toBeTruthy();
  }));
});
