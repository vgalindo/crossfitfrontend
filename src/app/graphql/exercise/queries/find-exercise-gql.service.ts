import { Injectable } from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class FindExerciseGqlService extends Query {

  document = gql`
    query exercises($id: String) {
      exercises(id: $id) {
        id
        name
        description
        picture
        video
      }
    }
`;
}
