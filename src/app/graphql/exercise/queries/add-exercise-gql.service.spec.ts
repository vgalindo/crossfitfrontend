import { TestBed, inject } from '@angular/core/testing';

import { AddExerciseGqlService } from './add-exercise-gql.service';

describe('AddExerciseGqlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddExerciseGqlService]
    });
  });

  it('should be created', inject([AddExerciseGqlService], (service: AddExerciseGqlService) => {
    expect(service).toBeTruthy();
  }));
});
