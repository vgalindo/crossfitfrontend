import { Injectable } from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class UpdateExercisesGqlService extends Mutation {
  document = gql`
    mutation updateExercises($id: String, $name: String, $description: String, $picture: String, $video: String) {
      updateExercise(id: $id, name: $name, description: $description, picture: $picture, video: $video) {
        id
        name
        description
        picture
        video
      }
    }
`;
}
