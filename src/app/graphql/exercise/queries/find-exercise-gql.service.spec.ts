import { TestBed, inject } from '@angular/core/testing';

import { FindExerciseGqlService } from './find-exercise-gql.service';

describe('FindExerciseGqlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FindExerciseGqlService]
    });
  });

  it('should be created', inject([FindExerciseGqlService], (service: FindExerciseGqlService) => {
    expect(service).toBeTruthy();
  }));
});
