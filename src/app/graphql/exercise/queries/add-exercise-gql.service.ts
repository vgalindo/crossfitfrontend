import { Injectable } from '@angular/core';
import {Mutation} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class AddExerciseGqlService extends Mutation {

  document = gql`
    mutation addExercises($name: String, $description: String, $picture: String, $video: String) {
      addExercise(name: $name, description: $description, picture: $picture, video: $video) {
        id
        name
        description
        picture
        video
      }
    }
`;
}


