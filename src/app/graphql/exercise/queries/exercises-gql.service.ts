import { Injectable } from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class ExercisesGQLService extends Query {

  document = gql`
    query allExercises{
      allExercises{
        id
        name
        description
        picture
        video
      }
    }
`;

}
