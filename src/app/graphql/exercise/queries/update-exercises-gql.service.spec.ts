import { TestBed, inject } from '@angular/core/testing';

import { UpdateExercisesGqlService } from './update-exercises-gql.service';

describe('UpdateExercisesGqlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdateExercisesGqlService]
    });
  });

  it('should be created', inject([UpdateExercisesGqlService], (service: UpdateExercisesGqlService) => {
    expect(service).toBeTruthy();
  }));
});
